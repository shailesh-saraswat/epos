<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Finalpayment extends Public_Layout
{
  public function __construct()
    {
        parent::__construct();
        $this->load->model('Userhubs');
        $this->load->model('Sites');
        $this->load->model('Pages');
        $this->load->model('Customers');
        $this->load->model('Cookie_Model');
        $this->load->model('Cart');
        $this->load->model('Tablebookings');
        $this->load->model('Dispatchers');
        $this->load->model('Contacts');
        //debugPrint($this->session->userdata('order_receive_date'));
        //$this->load->model('Sales');
        //debugPrint($this->Cart->get_order_receive_date());
    }
     public function payment()
     {
       $this->load->view("Payment/payment");
     }

     public function handlePayment()
    {
        require_once('application/libraries/stripe-php/init.php');
  
      $res=  \Stripe\Stripe::setApiKey($this->config->item('stripe_secret'));

      $result=Stripe\Charge::create ([
                "amount" => 100 * 120,
                "currency" => "INR",
                "source" => $this->input->post('stripeToken'),
                "description" => "Dummy stripe payment." 
        ]);
    
        $this->session->set_flashdata('success', 'Payment has been successful.');
        $this->Sites->checkout();
    }
}
?>
