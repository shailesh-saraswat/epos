<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class OnlineOffline extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        if ($this->server->require_scope()) {
            $this->load->model('Sales');
            $this->load->model('Cfgservices');
            $this->load->model('Tableplans');
        }
    }
   
    public function index_get()
    {
        // if($_GET['status_id']=="")
        // {
        $online_status=1;
        $offline_status=0;
        @$getstatusdata=$this->db->get_where("online_offline", array("id"=>1))->row();  
        if(@$getstatusdata->status===$online_status)
        {
         @$change_status=0;
        }
        else
        {
            @$change_status=1;
        }
         $this->db->update("online_offline", array("status"=>@$change_status), array("id"=>1));

         @$returndata=$this->db->get_where("online_offline", array("id"=>1))->row();
            if(@$returndata)
            {
                $results["success"]=@$returndata->status;
            }
           
            return $this->response($results); 
    //   }
    //   else{
    //       echo "failed";
    //   }
     

    }
}
